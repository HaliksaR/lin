const gulp = require("gulp"),
  loadPlugins = require("gulp-load-plugins"),
  plugins = loadPlugins();

console.log(plugins);

gulp.task("sass-to-css", () => {
  function run() {
    return gulp
      .src("sass/*.sass")
      .pipe(plugins.sass())
      .pipe(gulp.dest("css"));
  }
  plugins.watch("sass/*.sass", run);
  return run();
});

gulp.task("concat-css", () => {
  function run() {
    return gulp
      .src(["css/styles.css", "css/media.css", "css/keyframes.css"])
      .pipe(plugins.concat("style.concat.css"))
      .pipe(gulp.dest("css/css-concat"));
  }
  plugins.watch("css/*.css", run);
  return run();
});

gulp.task("autoprefixer", ["concat-css"], () => {
  function run() {
    return gulp
      .src("css/css-concat/*.css")
      .pipe(
        plugins.autoprefixer({
          browsers: ["last 5 versions"],
          cascade: false
        })
      )
      .pipe(gulp.dest("css/css-concat"));
  }
  plugins.watch("css/css-concat/*.css", run);
  return run();
});

gulp.task("default", ["sass-to-css", "autoprefixer"]);

gulp.task("html-replace", () => {
  gulp
    .src(["./index.html"])
    .pipe(
      plugins.replaceString(
        "css/css-concat/style.concat.css",
        "css/style.min.css"
      )
    )
    .pipe(plugins.replaceString("src/", "images/"))
    .pipe(gulp.dest("dist"));
});

gulp.task("compress-html", ["html-replace"], () => {
  return gulp
    .src("dist/*.html")
    .pipe(plugins.htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("dist"));
});

gulp.task("js-replace", () => {
  gulp
    .src(["js/*.js"])
    .pipe(plugins.replaceString("src/", "images/"))
    .pipe(gulp.dest("dist/js/"));
});

gulp.task("compress-js", ["js-replace"], function(cb) {
  gulp
    .src("dist/js/*.js")
    .pipe(plugins.jsMinify())
    .pipe(gulp.dest("dist/js/"));
});

gulp.task("compress-src", () => {
  return gulp
    .src("src/*")
    .pipe(plugins.imagemin())
    .pipe(gulp.dest("dist/images/"));
});

gulp.task("compress-css", () => {
  return gulp
    .src("css/css-concat/*.concat.css")
    .pipe(
      plugins.csso({
        comments: false
      })
    )
    .pipe(plugins.rename("style.min.css"))
    .pipe(gulp.dest("dist/css/"));
});

gulp.task("compress", [
  "compress-src",
  "compress-js",
  "compress-css",
  "compress-html"
]);

gulp.task("push", ["compress"]);

gulp.task("min-selectors", () => {
  return gulp
    .src(["css/css-min/*.css", "./*.html"])
    .pipe(plugins.selectors.run())
    .pipe(gulp.dest("dist"));
});
