$(document).ready(function() {
    $(".nav-menu-button").click(function() {
        $(".menu-childes").toggleClass("hidden");
        if (!$("#menu").hasClass("hidden")) {
            $("button > .nav-menu-logo").attr('src', 'src/close.svg');
        } else {
            $("button > .nav-menu-logo").attr('src', 'src/menu.svg');
        }
    });
});